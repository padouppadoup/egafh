// ======== Les fonctions du compteur principal
var tps=0; // Le compteur principal
var gens = [];
var gensId = 0;

var wooYayIntervalId = 0;
var tpsGenre = {Femme: 0,
		Homme: 0,
		Autre: 0};
var demarre = 0;
var gensParle = -1;
var genreParle = -1;
var heureDebut = "";
var dateDebut = "";

function mainTimerStartStop ( )
{
  if ( document.getElementById("btnTimer").value == "Démarrer" )
  {
      // Start the timer
      document.getElementById("btnTimer").value = "Pause";
      wooYayIntervalId = setInterval ( "chrono()", 1000 );
      if (demarre==0) {heureDebut=Date();};
      demarre = 1;
  }
  else
  {
      //document.getElementById("tpsFemme").innerHTML = "";
      document.getElementById("btnTimer").value = "Démarrer";
      clearInterval ( wooYayIntervalId );
      demarre = 0;
  }
}

// ======= Les fonctions pour ajouter des lignes
function ajouterGens() {
    // Ajouter à l'objet général
    var genre = document.getElementById("genre").value;
    var nom = document.getElementById("nom").value;
    var titre = document.getElementById("titre").value;
    var gen = {nom: nom, genre: genre, titre: titre,
	       interventions: [], arrivee: tps, depart: -1, temps: 0,
	       parle: 0};
    var id = gensId;
    gens.push(gen);
    
    //alert(gen.genre+", "+gen.nom+", "+gen.arrivee);

    // Écrire dans la table des gens
    $("#tableGens").find("tbody").append($("<tr>")
					 .attr("id", "gens"+id)
					 .append($("<input>")
						 .attr("id", "ggens"+id)
						 .attr("type", "button")
						 .attr("alt", "Quitte la réunion")
						 .attr("value", "X")
						 .attr("onclick", "enleverGens(this)"))
					 .append("["+genre.substring(0,1)+"]&nbsp;")
					 .append($("<em>").text(nom))
					 .append("&nbsp;("+titre+")&nbsp;")
					 .append($("<em>")
						 .attr("id", "tps"+id)
						 .text("00:00:00"))
					 .append($("<input>")
							    .attr("class", "intervention")
							    .attr("type", "button")
							    .attr("value", "Parle")
							    .attr("onclick", "intervention(this,0)")
							    .attr("id", "ttt"+id))
						    .append($("<input>")
							    .attr("class", "interventionP")
							    .attr("type", "button")
							    .attr("value", "Parle (préparé)")
							    .attr("onclick", "intervention(this,1)")
							    .attr("id", "ttt"+id))
						    .append($("<input>")
							    .attr("class", "commentaire")
							    .attr("value", "Commentaire")
							    .attr("id", "comm"+id))
					)    

    // Vider le formulaire
    gensId +=1;
    document.getElementById("nom").value = "";
    document.getElementById("titre").value = "";
    
}

var tmp;
function enleverGens(ttsss) {
    //this.parentNode.style.background-color = "#f00";
    var node = document.getElementById(ttsss.id).parentNode;
    var i1=node.getElementsByClassName("intervention");
    var i2=node.getElementsByClassName("interventionP");
    i1[0].disabled="disabled";
    i2[0].disabled="disabled";
    idd=ttsss.numero.substring(5,100);
    gens[idd].depart=tps;
    console.log("Le gens '"+gens[idd].nom+ "' a été enlevé de la liste");
    // Griser les machins
}

// ======= Des fonctions pour gérer le chrono
function intervention(idds, prepare) {
    if (demarre == 0) {alert("La réunion n'est pas démarrée ou en pause");return -1};
    idd=idds.id.substring(3,20);
    console.log(idd);
    // Dire qui se tait et qui parle
    if (gensParle != -1) {
	// Quelqu'un (celui qui parlait) se tait
	gens[gensParle].parle = 0
	// Mettre le label à 0
	var node = document.getElementById("gens"+gensParle);
	var i1=node.getElementsByClassName("intervention");
	var i2=node.getElementsByClassName("interventionP");
	i1[0].value = "Parle";
	i2[0].value = "Parle (préparé)";
	// Figer le temps
	finParler(gensParle);
    }
    if (gensParle == idd) {
	// Plus personne ne parle
	gensParle = -1;
    } else {
	gensParle = idd;
	gens[idd].parle = 1;
	var node = document.getElementById("gens"+gensParle);
	var i1=node.getElementsByClassName("intervention");
	var i2=node.getElementsByClassName("interventionP");
	if (prepare == 0) {i1[0].value = "Terminé";}
	if (prepare == 1) {i2[0].value = "Terminé";}
	debutParler(idd, prepare);
    }
}
function finParler(idd) {
    console.log("Arreter de parler " + idd);
    // Enregistrer le commentaire
    var comm = document.getElementById("comm"+idd).value;
    var l=gens[idd].interventions.length;
    if ((comm != "Commentaire") & (comm != "")) {
	gens[idd].interventions[l-1].commentaire = comm;
    }
    gens[idd].interventions[l-1].fin = tps;
    document.getElementById("comm"+idd).value = "";
}
function debutParler(idd, prepare) {
    console.log("Début de parler " + idd);
    gens[idd].interventions.push({debut: tps,
				  fin: -1,
				  prepare: prepare,
				  commentaire: -1}); // Créer l'objet
    // Gérer les chronos totaux aussi
}

// ======== Des fonctions pour gérer l'export
function terminerReunion() {
    var out = {date: document.getElementById("thedate").innerHTML,
	       heure: document.getElementById("heuretheo").value,
	       heureDebut: heureDebut,
	       tempsTotaux: tpsGenre,
	       temps: tps,
	       tempsDetail: gens};
    var filename = document.getElementById("outfile").value + ".json";
    saveTextAsFile(JSON.stringify(out, null, 4), filename);
    alert("Recharger la page pour un chronomètre vierge");
}

function saveTextAsFile(textToWrite, fileNameToSaveAs)
// Pompé de http://thiscouldbebetter.wordpress.com/2012/12/18/loading-editing-and-saving-a-text-file-in-html5-using-javascrip/
{
	//var textToWrite = document.getElementById("inputTextToSave").value;
	var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
	//var fileNameToSaveAs = document.getElementById("inputFileNameToSaveAs").value;

	var downloadLink = document.createElement("a");
	downloadLink.download = fileNameToSaveAs;
	downloadLink.innerHTML = "Download File";
	if (window.webkitURL != null)
	{
		// Chrome allows the link to be clicked
		// without actually adding it to the DOM.
		downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
	}
	else
	{
		// Firefox requires the link to be added to the DOM
		// before it can be clicked.
		downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
		downloadLink.onclick = destroyClickedElement;
		downloadLink.style.display = "none";
		document.body.appendChild(downloadLink);
	}

	downloadLink.click();
}
function destroyClickedElement(event)
{
	document.body.removeChild(event.target);
}

// ========  Des fonctions qui trainent
function chrono ( )
{
    tps += 1;
    //document.getElementById("tpsFemme").innerHTML = tpsFemme;
    document.getElementById("tpsTot").innerHTML = toHHMMSS(tps);

    // Incrémenter le temps de la personne qui parle
    if (gensParle != -1) {
	gens[gensParle].temps+=1;
	document.getElementById("tps"+gensParle).innerHTML = toHHMMSS(gens[gensParle].temps)
	// Incrémenter le temps du genre
	var genr = gens[gensParle].genre;
	tpsGenre[genr]+=1;
	document.getElementById("tps"+genr).innerHTML = toHHMMSS(tpsGenre[genr]);
    }
}

function toHHMMSS(sec_num) {
    var hours = Math.floor(sec_num/3600);
    var minutes = Math.floor((sec_num-(hours*3600))/60);
    var seconds = sec_num - (hours*3600) - (minutes*60);

    if (hours<10) {hours = "0"+hours;}
    if (minutes<10) {minutes = "0"+minutes;}
    if (seconds<10) {seconds = "0"+seconds;}
    var time = hours+":"+minutes+":"+seconds;
    return time;
}

function theDate() {
    var cd = new Date();
    var day = cd.getDay()+2;
    var month = cd.getMonth()+1;
    var year = cd.getFullYear();
    //return day + "/" + month + "/" + year;
    document.getElementById("thedate").innerHTML = day + "/" + month + "/" + year;
}
