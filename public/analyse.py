#-*-coding:utf-8-*-
# MW, Écocampus (http://ecocampus.ens.fr) pour le REFEDD
# Nov 2014, GPLv3+

# ==== Importations
import json, sys

# ==== Variables
#path = "../data/141108_CA_8-novembre-2014.json"
#path = "../data/141108_CA_samediMatin.json"
path = "../data/141109_CA-Dim-matin.json"
#path = sys.argv[1]

# ==== Fonctions
def traiterPersonne(o) :
    nom = o["nom"]
    genre = o["genre"]
    titre = o["titre"]
    arrivee = o["arrivee"]
    depart = o["depart"]
    temps = o["temps"]

    ret = u""
    for i in o["interventions"] :
        ret += u"{}; {}; {}; {}; {}; {};".format(nom, genre, titre, arrivee, depart, temps)
        ret += u"{}; {}; {}; {}\n".format(i["debut"], i["fin"], i["prepare"], i["commentaire"])
    return ret

# ==== Lecture de l'objet
o = open(path, "r").read()
data = json.loads(o)

general = u"Heure de début; {}\n".format(data["heureDebut"])
general += u"Temps total (s); {}\n".format(data["temps"])
general += u"Temps autre (s); {}\n".format(data["tempsTotaux"]["Autre"])
general += u"Temps femme (s); {}\n".format(data["tempsTotaux"]["Femme"])
general += u"Temps homme (s); {}\n".format(data["tempsTotaux"]["Homme"])

detail = u"Nom; Genre; Titre; Arrivée; Départ; Temps; Début; Fin; Préparé?; Commentaire\n"
for i in data["tempsDetail"] :
    detail += traiterPersonne(i)

print detail
print
print general
#out = open(path+".csv", "w")
#out.write(general+u"\n"+detail)
#out.close()
